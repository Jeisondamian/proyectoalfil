/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author USUARIO
 */
public class Peon extends Ficha{


    public Peon(int i, int j) {
        super(i, j);
    }

    public void mover(boolean dir){
        if(dir==true){
            this.setPosicion(this.getI()+1, this.getJ());
        }else{
            this.setPosicion(this.getI()-1, this.getJ());
        }
    }
    
    public int proxMovimientoPeon(boolean dir){
        if(!dir)     
            return this.getI()-1;
        else     
            return this.getI()+1;
    }
}
