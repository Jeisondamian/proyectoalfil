/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madarme
 */
public class Ficha {
    
    protected int[] posicion;

    public Ficha() {
        this.posicion = new int[2];
    }

    public Ficha(int i, int j) {
        this.posicion = new int[2];
        this.posicion[0] = i;
        this.posicion[1] = j;
    }
    
    public void setPosicion(int i, int j){
        this.posicion[0] = i;
        this.posicion[1] = j;
    }
    
    public int getI(){
        return this.posicion[0];
    }
    
    public int getJ(){
        return this.posicion[1];
    }

    public int[] getPosicion() {
        return posicion;
    }
    
    public void mover(){
    
    }


}
