/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author USUARIO
 */
public class Alfil extends Ficha {

    public Alfil(int i, int j) {
        super(i, j);
    }

    public void mover(int[] posicionPeon, boolean dir) {

        int i_alfil = this.getI();
        int j_alfil = this.getJ();
        int i_peon = posicionPeon[0];
        int j_peon = posicionPeon[1];
        Casilla c = new Casilla(0, 0);
        if (dir) {
            if (i_peon > i_alfil) {
                if (j_peon > j_alfil) {
                    if (i_alfil != 0) {
                        this.setPosicion(--i_alfil, ++j_alfil);
                    } else {
                        this.setPosicion(++i_alfil, ++j_alfil);
                    }
                } else {
                    if (i_alfil != 0) {
                        this.setPosicion(--i_alfil, --j_alfil);
                    } else {
                        this.setPosicion(++i_alfil, --j_alfil);
                    }
                }
            } else {
                if (j_peon > j_alfil) {
                    c.setI(i_alfil - 1);
                    c.setJ(j_alfil + 1);
                    if (c.esCasillaValida()) {
                        this.setPosicion(--i_alfil, ++j_alfil);
                    } else {
                        c.setI(i_alfil + 1);
                        c.setJ(j_alfil - 1);
                        if (c.esCasillaValida()) {
                            this.setPosicion(++i_alfil, --j_alfil);
                        } else {
                            this.setPosicion(++i_alfil, ++j_alfil);
                        }

                    }
                } else if (j_alfil == j_peon) {
                    if (j_peon < 4) {
                        this.setPosicion(--i_alfil, ++j_alfil);
                    } else {
                        this.setPosicion(--i_alfil, --j_alfil);
                    }
                } else {//here
                    c.setI(i_alfil - 1);
                    c.setJ(j_alfil - 1);
                    if (c.esCasillaValida()) {
                        this.setPosicion(--i_alfil, --j_alfil);
                    } else {
                        c.setI(i_alfil + 1);
                        c.setJ(j_alfil + 1);
                        if (c.esCasillaValida()) {
                            this.setPosicion(++i_alfil, ++j_alfil);
                        } else {
                            this.setPosicion(++i_alfil, --j_alfil);
                        }
                    }
                }
            }

        } else {
            if (i_peon > i_alfil) {
                if (j_peon > j_alfil) {
                    if (j_alfil != 0) {
                        this.setPosicion(--i_alfil, --j_alfil);
                    } else {
                        this.setPosicion(++i_alfil, ++j_alfil);
                    }
                } else if (j_alfil == j_peon) {
                    if (j_peon < 4) {
                        this.setPosicion(++i_alfil, ++j_alfil);
                    } else {
                        this.setPosicion(++i_alfil, --j_alfil);
                    }
                } else {
                    if (j_alfil != 7) {
                        this.setPosicion(++i_alfil, ++j_alfil);
                    } else {
                        this.setPosicion(++i_alfil, --j_alfil);
                    }
                }
            } else {
                if (j_peon > j_alfil) {
                    c.setI(i_alfil + 1);
                    c.setJ(j_alfil + 1);
                    if (c.esCasillaValida()) {
                        this.setPosicion(++i_alfil, ++j_alfil);
                    } else {
                        c.setI(i_alfil - 1);
                        c.setJ(j_alfil - 1);
                        if (c.esCasillaValida()) {
                            this.setPosicion(--i_alfil, --j_alfil);
                        } else {
                            this.setPosicion(--i_alfil, ++j_alfil);
                        }
                    }
                } else {
                    c.setI(i_alfil + 1);
                    c.setJ(j_alfil - 1);
                    if (c.esCasillaValida()) {
                        this.setPosicion(++i_alfil, --j_alfil);
                    } else {
                        c.setI(i_alfil - 1);
                        c.setJ(j_alfil + 1);
                        if (c.esCasillaValida()) {
                            this.setPosicion(--i_alfil, ++j_alfil);
                        } else {
                            this.setPosicion(--i_alfil, --j_alfil);
                        }

                    }
                }
            }
        }
    }
}
