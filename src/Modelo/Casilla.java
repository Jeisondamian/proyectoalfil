/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author USUARIO
 */
public class Casilla {
    protected Ficha ficha = new Ficha();
    protected int i=0, j=0;

    public Casilla() {

    }

    public Casilla(Ficha ficha) {
        this.ficha = ficha;
        this.i = ficha.getI();
        this.j = ficha.getJ();
    }
    
    public Casilla(int i, int j) {
        this.i=i;
        this.j=j;
    }

    public Ficha getFicha() {
        return ficha;
    }

    public void setFicha(Ficha ficha) {
        this.ficha = ficha;
    }
    
    public boolean isOcupada(){
        return ficha!=null;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public int getJ() {
        return j;
    }

    public void setJ(int j) {
        this.j = j;
    }
    
    /*  Metodo que valida la casilla, es decir, si su i,j está entre los limites del tablero */
    public boolean esCasillaValida(){
        return this.getI()>=0 && this.getI()<=7 && this.getJ()>=0 && this.getJ()<=7;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Casilla other = (Casilla) obj;
        if (this.i != other.i) {
            return false;
        }
        if (this.j != other.j) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Casilla{" + "i=" + i + ", j=" + j + '}';
    }
    
}
