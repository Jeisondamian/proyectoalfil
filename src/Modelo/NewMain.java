/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Negocio.Tablero;
import com.itextpdf.text.DocumentException;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

/**
 *
 * @author USUARIO
 */
public class NewMain {

    /**
     * @param args the command line arguments
     * @throws java.net.MalformedURLException
     * @throws com.itextpdf.text.DocumentException
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws MalformedURLException, DocumentException, FileNotFoundException, IOException {
        Tablero t = new Tablero(7,1,7,0,false);
        t.jugar();
        File path = new File ("src/Negocio/Solucion.pdf");
        Desktop.getDesktop().open(path);
        //System.out.println(""+t.getSolucion()+"\n");        
    }
    
}
