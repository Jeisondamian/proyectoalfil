/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;


import Modelo.Alfil;
import Modelo.Casilla;
import Modelo.Ficha;
import Modelo.Peon;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import com.itextpdf.text.Phrase;
import java.io.IOException;
import ufps.util.colecciones_seed.IteratorLS;
import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author madar
 */
public class Tablero {
    
    
    private Casilla[][] myTablero;
    private Alfil a;
    private Peon p;
    private boolean dirPeon;
    private String[] ruta;
    private ListaS<PdfPTable> tables;
    
    //Adicione los atributos que considere necesarios para el correcto fucnioanmiento de su aplicación, si y solo si , no violen ninguna regla en POO
    public Tablero() {
        tables = new ListaS <>();
        this.ruta = new String[20];
        this.myTablero = new Casilla[8][8];
       
    }
    
    /**
     * Constructor para iniciar el juego
     * @param i_alfil Posición de la fila para el alfil
     * @param j_alfil Posición de la columna para el alfil
     * @param i_peon  Posición de la fila para el peon
     * @param j_peon  Posición de la columna para el alfil
     * @param dirPeon true si el peón se mueve de arriba hacia abajo, o false en caso contrario
     * @throws java.net.MalformedURLException
     * @throws com.itextpdf.text.DocumentException
     * @throws com.itextpdf.text.BadElementException
     */
    
    public Tablero(int i_alfil, int j_alfil, int i_peon,int j_peon, boolean dirPeon) throws MalformedURLException, DocumentException, BadElementException, IOException {
        this.myTablero = new Casilla[8][8];
        this.dirPeon = dirPeon;
        a = new Alfil(i_alfil, j_alfil);
        p = new Peon(i_peon, j_peon);
        myTablero[i_alfil][j_alfil] = new Casilla(a);
        myTablero[i_peon][j_peon]= new Casilla(p);
        this.tables = new ListaS<>();
        this.initRuta();
    }
    
    /*  Metodo de ejecuta el Juego */
    public void jugar() throws MalformedURLException, DocumentException, FileNotFoundException, IOException, BadElementException
    {
        if(!estaEnElFinal()){
            if(esPosibleMoverPeon()){
                p.mover(dirPeon);
                updateMatriz(p);
                int i=p.getI()+1;
                int j=p.getJ()+1;
                this.aggMovimiento("Peon["+i+","+j+"] ->");
            } else {
                a.mover(p.getPosicion(), dirPeon);
                updateMatriz(a);
                int i=a.getI()+1;
                int j=a.getJ()+1;
                this.aggMovimiento("Alfil["+i+","+j+"] ->");
            }
        tables.insertarAlFinal(this.getTablePdf());
        jugar();
        } else {
            this.getSolucionPdf();
        }
    }
    
    /* metodo que actualiza las posiciones de las fichas en la Matriz */
    private void updateMatriz(Ficha f){
        myTablero[f.getI()][f.getJ()] = new Casilla(f);
    }
    
    /*  Metodo que valida si el Peon puede avanzar a la siguiente casilla */
    public boolean esPosibleMoverPeon(){ //j=x i=y
        int y = -a.getI();
        int x = a.getJ();
        int k1 = y-x;
        int k2 = y+x;
        
        Casilla c = new Casilla(-1*(p.getJ()+k1), p.getJ());
        Casilla c1 = new Casilla(-1*(k2-p.getJ()), p.getJ());
        //System.out.println(""+c.getI()+", "+c.getJ());
        //System.out.println(""+c1.getI()+", "+c1.getJ());
        
        if(c.esCasillaValida() && c1.esCasillaValida()){
            if(c.getI() != p.proxMovimientoPeon(dirPeon))
                return c1.getI() != p.proxMovimientoPeon(dirPeon);
            else
                return false;
        }
        if(c.esCasillaValida()){
            return c.getI() != p.proxMovimientoPeon(dirPeon);
        }
        if(c1.esCasillaValida()){
            return c1.getI() != p.proxMovimientoPeon(dirPeon);
        } else 
            return true;
    }
    
    /*Metodo que inicializa la cadena que contendrá las movimientos efectuados por las fichas*/
    private void initRuta() throws MalformedURLException, DocumentException, BadElementException, IOException{
        ruta=new String[20];
        int i, j;
        i=1+a.getI();
        j=1+a.getJ();
        String mov="Alfil["+i+","+j+"] ->";
        i=p.getI()+1;
        j=p.getJ()+1;
        mov+="Peon["+i+","+j+"] ->";
        aggMovimiento(mov);
        tables.insertarAlFinal(this.getTablePdf());
    }

    /*Metodo que valida si el peon se encuentra en su meta*/
    private boolean estaEnElFinal() {
        if(dirPeon)
            if(p.getI()==7)
                return true;
        if(!dirPeon)
            if(p.getI()==0)
                return true;
        return false;
    }
    
    /*Metodo que genera un archivo pdf con la solución del ejercicio*/
    public void getSolucionPdf() throws FileNotFoundException, MalformedURLException {
        try {
            Document doc = new Document();
            FileOutputStream file = new FileOutputStream("src/Negocio/Solucion.pdf");
            PdfWriter.getInstance(doc, file);
            
            doc.open();
            //imprimir listaS
            Paragraph parrafo = new Paragraph();
            Paragraph parrafo0 = new Paragraph();
            //parrafo.setAlignment(1000000);
            parrafo.add(" \n"+" \n"+" \n"+" ");
            IteratorLS<PdfPTable> jugada = (IteratorLS<PdfPTable>) tables.iterator();
            
            for(int i=0; i<tables.getTamanio(); i++){
                doc.add(jugada.next());
                if(i==tables.getTamanio()-1)
                    this.ruta[i]+="Fin";
                parrafo0.add(this.ruta[i]);
                if(i==6)
                    parrafo = new Paragraph(" \n"+" \n");
                doc.add(parrafo0);
                doc.add(parrafo);
            }
            doc.close();
            
        } catch (DocumentException ex) {
            //Logger.getLogger(Tablero.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("No se encontró el destino");
        }
    }
    
    /*Metodo que construye una tabla tipo PdfPTable y la retorna */
    private PdfPTable getTablePdf() throws MalformedURLException, DocumentException, BadElementException, IOException{
        
        Image peon = Image.getInstance("src/Vista/peon50.png");
        Image alfil = Image.getInstance("src/Vista/alfil50.png");
        
        PdfPTable table = new PdfPTable(8);
        PdfPCell cell = new PdfPCell();
        //cell.setBorder(0);
        Font font = new Font(FontFamily.TIMES_ROMAN, 12, Font.BOLDITALIC, BaseColor.WHITE);
        boolean blanco=false;
       for(int i=0; i<8; i++){
            blanco=!blanco;
            for(int j=0; j<8; j++){
                if(i==p.getI() && j==p.getJ())
                    cell.setImage(peon);
                if(i==a.getI() && j==a.getJ())
                    cell.setImage(alfil);
                if(blanco){
                    cell.setBackgroundColor(BaseColor.WHITE);
                }else{
                    cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                }
                cell.setFixedHeight(38f);
                cell.setBorderColor(BaseColor.LIGHT_GRAY);
                table.addCell(cell);
                cell.setPhrase(new Phrase(" "));
                blanco=!blanco;
            }
        }
        return table;
    }
    
    public void aggMovimiento(String mov){
        for(int i=0; i<ruta.length; i++){
            if(ruta[i]==null){
                ruta[i]=mov;
                break;
            }
        }
    }
    
}
